== Basic data visualisation
=== Scalable vector graphics
Knowing how HTML, CSS and javascript work, we can start making our visualisations. There are two main approaches for making visualisations in a webbrowser. You can either tell the computer which pixels need to have which colour (_pixel-based_), or you can tell it at a higher level that you want a circle at a certain position on the screen (_vector-based_). Although the pixel-based approach gives more flexibility and control, it can be much harder to create these visuals and interact with them. In addition, vector-based visuals are _scalable_. The image below shows the difference between vector- and pixel-based circle after zooming:

image:canvas_vs_svg.jpg[width=50%,pdfwidth=50%]

We will be using SVG (scalable vector graphics) for this tutorial. A plot is a collection of `<circle>`, `<rect>`, etc elements that are contained within a single `<svg>` element. They are basically like any other HTML element. Here is a simple plot of 300x300 pixels, containing a circle and a rectangle.

[source,html,linenums]
----
<html>
<body>
    <svg width="300" height="300">
        <circle cx="20" cy="50" r="30"/>
        <rect x="100" y="200" width="70" height="30" />
    </svg>
</body>
----

IMPORTANT: The *origin `(0,0)`* of an SVG image is at the *top left*, not the bottom left. This means that higher values for `y` will give you marks that are lower in the graphic, as you can see with the circle and the rectangle below. When plotting actual data, make sure that you flip that orientation.

As these are regular HTML elements, we can style them using CSS as well:

[source,html,linenums]
----
<html>
<head>
    <style>
        svg {
            border: 1px;
            border-style: solid;
        }
        circle {
            fill: steelblue;
            opacity: 0.5;
        }
        circle:hover {
            fill: red;
            opacity: 1;
        }
        rect {
            fill: green;
            stroke: red;
        }
    </style>
</head>
<body>
    <svg width="300" height="300">
        <circle cx="20" cy="50" r="30"/>
        <rect x="100" y="200" width="70" height="30" />
    </svg>
</body>
----

We give the SVG itself a border. Any circle should be blue and 50% transparent, except when we hover over it when it should become red and fully opaque. Rectangles should be green with a red outline. This is the resulting graphic (see what happens when you hover over the circle):

ifndef::backend-pdf[]
++++
<style>
    svg {
        border: 1px;
        border-style: solid;
    }
    circle.figone {
        fill: steelblue;
        opacity: 0.5;
    }
    circle.figone:hover {
        fill: red;
        opacity: 1;
    }
    rect {
        fill: green;
        stroke: red;
    }
</style>
<svg width="300" height="300">
    <circle class="figone" cx="20" cy="50" r="30"/>
    <rect x="100" y="200" width="70" height="30" />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg1.png[width=25%,pdfwidth=25%]
endif::[]

There are a large number of elements that can be used within an SVG element. These include `circle`, `rect`, `line`, `path`, etc. Each has their own list of possible attributes. For a full overview, see https://developer.mozilla.org/en-US/docs/Web/SVG/Element/svg[https://developer.mozilla.org/en-US/docs/Web/SVG/Element/svg]. Definitely check out that resource.

==== A simple scatterplot
Let's say we have 5 datapoints that we want ot put into a scatterplot: `[[20,100],[60,140],[80,20],[160,40],[180,160]]`. We can make a plot like this:

[source,html,linenums]
----
<html>
<head>
    <style>
        svg {
            border: 1px;
            border-style: solid;
        }
        circle {
            fill: steelblue;
        }
    </style>
</head>
<body>
    <svg width="200" height="200">
        <circle cx="20" cy="100" r="10"/>
        <circle cx="60" cy="140" r="10"/>
        <circle cx="80" cy="20" r="10"/>
        <circle cx="160" cy="40" r="10"/>
        <circle cx="180" cy="160" r="10"/>
    </svg>
</body>
</html>
----

ifndef::backend-pdf[]
++++
<style>
    svg {
        border: 1px;
        border-style: solid;
    }
    .figtwo {
        fill: steelblue;
    }
</style>
<svg width="200" height="200">
    <circle class="figtwo" cx="20" cy="100" r="10"/>
    <circle class="figtwo" cx="60" cy="140" r="10"/>
    <circle class="figtwo" cx="80" cy="20" r="10"/>
    <circle class="figtwo" cx="160" cy="40" r="10"/>
    <circle class="figtwo" cx="180" cy="160" r="10"/>
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg2.png[width=25%,pdfwidth=25%]
endif::[]

==== Rectangle, ellipse, line
Other straightforward marks available to you in SVG are rectangles, ellipses, and lines.

TIP: Make sure to bookmark the Mozilla MDN Docs at https://developer.mozilla.org/en-US/docs/Web/SVG. You can find information there on what parameters are available for which shapes, how to draw text, transformations and coordinate systems, etc.

[source,html,linenums]
----
<style>
	line {
		stroke: black;
		stroke-width: 3;
	}
	ellipse {
		fill: steelblue;
	}
	rect {
		fill: red;
	}
</style>

<svg width="200" height="200">
    <ellipse cx="20" cy="100" rx="10" ry="20" />
    <rect x="60" y="140" width="20" height="10" />
    <line x1="80" y1="10" x2="40" y2="50" />
</svg>
----

ifndef::backend-pdf[]
++++
<style>
    svg {
        border: 1px;
        border-style: solid;
    }
	line.fig4 {
		stroke: black;
		stroke-width: 3;
	}
	ellipse.fig4 {
		fill: steelblue;
	}
	rect.fig4 {
		fill: red;
	}
</style>

<svg width="200" height="200">
    <ellipse class="fig4" cx="20" cy="100" rx="10" ry="20" />
    <rect class="fig4" x="60" y="140" width="20" height="10" />
    <line class="fig4" x1="80" y1="10" x2="40" y2="50" />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg3.png[width=25%,pdfwidth=25%]
endif::[]

==== Polylines and polygons
If we want to create a jagged line, we can add multiple lines together when one starts where another ends, like so:

[source,html,linenum]
----
<svg width="200" height="200">
    <line x1="0" y1="200" x2="100" y2="50" />
    <line                 x1="100" y1="50" x2="100" y2="150" />
    <line                                  x1="100" y1="150" x2="200" y2="0" />
</svg>
----
(I've added whitespace here to indicate that the startpoints of each line are the same as the endpoints of the previous line.)

Of course it's nicer to do that in one go, like so:

[source,html,linenum]
----
<style>
    polyline {
        fill: none;
        stroke: black;
        stroke-width: 3px;
    }
</style>

<svg width="200" height="200">
    <polyline points="0,200 100,50 100,150 200,0" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <polyline points="0,200 100,50 100,150 200,0"
        style="fill:none; stroke: black; stroke-width: 3px"
    />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg4.png[width=25%,pdfwidth=25%]
endif::[]

Note that we defined `fill` as `none`. If we give the element a fill colour, it act as if the start and end point are connected. For example:

[source,html,linenum]
----
<style>
    polyline {
        fill: green;
        stroke: black;
        stroke-width: 3px;
    }
</style>

<svg width="200" height="200">
    <polyline points="0,200 100,50 100,150 200,0" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <polyline points="0,200 100,50 100,150 200,0"
        style="fill:green; stroke: black; stroke-width: 3px"
    />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg5.png[width=25%,pdfwidth=25%]
endif::[]

A `polygon` is almost exactly the same as a `polyline`, but it connects the start and end points.

[source,html,linenum]
----
<style>
    polygon {
        fill: none;
        stroke: black;
        stroke-width: 3px;
    }
</style>

<svg width="200" height="200">
    <polygon points="0,200 100,50 100,150 200,0" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <polygon points="0,200 100,50 100,150 200,0"
        style="fill:none; stroke: black; stroke-width: 3px"
    />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg6.png[width=25%,pdfwidth=25%]
endif::[]

[#paths]
==== Paths
Any of the marks shown above can also be created using a `path`. The Mozilla MDN Docs have pretty good https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths[tutorial] on them.

A path has one argument, `d` which is a string that describes the path. At its most basic, we can move (`M` or `m`) the mouse (without drawing), and draw a lines (`L` or `l`). We can recreate the polyline from above like so:

[source,html,linenums]
----
<style>
    path {
        fill: none;
        stroke: black;
        stroke-width: 3px;
    }
</style>

<svg width="200" height="200">
    <path d="M 0,200 L 100,50 L 100,150 L 200,0" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <path d="M 0,200 L 100,50 L 100,150 L 200,0"
        style="fill:none; stroke: black; stroke-width: 3px"
    />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg7.png[width=25%,pdfwidth=25%]
endif::[]

We can close the path (and change the polyline to a polygon) by adding a `Z` at the end of the string: `<path d="M 0,200 L 100,50 L 100,150 L 200,0 Z" />`.

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <path d="M 0,200 L 100,50 L 100,150 L 200,0 Z"
        style="fill:none; stroke: black; stroke-width: 3px"
    />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg8.png[width=25%,pdfwidth=25%]
endif::[]

So `M` moves the pen across the paper without touching the paper; `L` does so with touching the paper. We can illustrate this by replacing one of the `L` s above into an `M`.

[source,html,linenums]
----
<style>
    path {
        fill: none;
        stroke: black;
        stroke-width: 3px;
    }
</style>

<svg width="200" height="200">
    <path d="M 0,200 L 100,50 M 100,150 L 200,0" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <path d="M 0,200 L 100,50 M 100,150 L 200,0"
        style="fill:none; stroke: black; stroke-width: 3px"
    />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg9.png[width=25%,pdfwidth=25%]
endif::[]

===== Relative positions
The coordinates given for `M` and `L` are the absolute coordinates within the SVG element. We can also give positions that are _relative_ to the current position. We do this using the lowercase `m` and `l`. This means that `M 50,50 L 50,100 L 100,50` is the same as `M 50,50 l 0,50 l 50,-50`.

[#curves]
==== Curves
The `path` element can also be used to draw curves like bezier curves and arcs.

===== Bezier curves
In SVG we can draw quadratic (`Q`) and cubic (`C`) bezier curves. A *quadratic bezier curve* is an interpolation between two linear interpolations. In the image below, \(P_0\) and \(P_2\) are the _anchor points_ and \(P_1\) is a _control point_. We interpolate between \(P_0\) and \(P_1\) on one hand, and \(P_1\) and \(P_2\) on the other. We connect those interpolations with a (green) line. Next, we interpolate across that green line to get the bezier curve itself.

ifndef::backend-pdf[]
image:quadratic_bezier.gif[width=50%,pdfwidth=50%]
endif::[]

ifdef::backend-pdf[]
image:quadratic_bezier_static.png[width=50%,pdfwidth=50%]
endif::[]

[.small]#Source: https://en.wikipedia.org/wiki/Bézier_curve#

In an SVG `path`, the \(P_0\) is the last point in the path, followed by the control point \(P_1\), and second anchor point \(P_2\). In the example below, \(P_0\) is `20,100`, \(P_1\) is `100,20`, and \(P_2\) is `180,100`. We also draw the points themselves as a reference (control point is in red).

[source,html,linenums]
----
<style>
    path {
        stroke: black;
        stroke-width: 3px;
        fill: none;
    }
</style>

<svg width="200" height="200">
    <path d="M 20,100 Q 100,20 180,100" />

    <circle cx="20" cy="100" r="5" style="fill: steelblue" />
    <circle cx="100" cy="20" r="5" style="fill: red" />
    <circle cx="180" cy="100" r="5" style="fill: steelblue" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <path d="M 20,100 Q 100,20 180,100"
        style="stroke: black; stroke-width: 3px; fill: none;"/>
    <circle cx="20" cy="100" r="5" style="fill: steelblue; stroke: none" />
    <circle cx="100" cy="20" r="5" style="fill: red; stroke: none" />
    <circle cx="180" cy="100" r="5" style="fill: steelblue; stroke: none" />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg10.png[width=25%,pdfwidth=25%]
endif::[]

A *cubic bezier curve* takes this one level higher, and therefore uses two control points.

ifndef::backend-pdf[]
image:cubic_bezier.gif[width=50%,pdfwidth=50%]
endif::[]

ifdef::backend-pdf[]
image:cubic_bezier_static.png[width=50%,pdfwidth=50%]
endif::[]

[.small]#Source: https://en.wikipedia.org/wiki/Bézier_curve#

An example:

[source,html,linenums]
----
<style>
    path {
        stroke: black;
        stroke-width: 3px;
        fill: none;
    }
</style>

<svg width="200" height="200">
    <path d="M 20,100 C 75,20 125,180 180,100" />
    <circle cx="20" cy="100" r="5" style="fill: steelblue;" />
    <circle cx="75" cy="20" r="5" style="fill: red;" />
    <circle cx="125" cy="180" r="5" style="fill: red;" />
    <circle cx="180" cy="100" r="5" style="fill: steelblue;" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <path d="M 20,100 C 75,20 125,180 180,100"
        style="stroke: black; stroke-width: 3px; fill: none;"/>
    <circle cx="20" cy="100" r="5" style="fill: steelblue; stroke: none" />
    <circle cx="75" cy="20" r="5" style="fill: red; stroke: none" />
    <circle cx="125" cy="180" r="5" style="fill: red; stroke: none" />
    <circle cx="180" cy="100" r="5" style="fill: steelblue; stroke: none" />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg11.png[width=25%,pdfwidth=25%]
endif::[]

===== Arcs
Arcs are tricky in SVG, so definitely check out the `path` tutorial at https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths if you need them.

As with the bezier curves, a curve starts from a certain point that is already defined in the `path`. So you will need for example a `M` or `L` directive before the arc itself. The arc is drawing using the `A` directive, which takes the following arguments:

- radius in x-direction `rx`
- radius in y-direction `ry`
- rotation around x axis
- `large-arc-flag`
- `sweep-flag`
- x coordinate of end point
- y coordinate of end point

For example:

[source,html,linenums]
----
<style>
    path {
        fill: orange;
        stroke: black;
        stroke-width: 3px;
    }
</style>

<svg width="200" height="200">
	<path d="M 50 50 A 2 1 -45 0 1 50 150" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
	<path d="M 50 50 A 2 1 -45 0 1 50 150" style="fill:orange; stroke: black; stroke-width: 3px;" />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg12.png[width=25%,pdfwidth=25%]
endif::[]

Notice that the radii `rx` and `ry` are scaled so that the arc can reach the end point.

[source,html,linenums]
----
<style>
    path {
        stroke: red;
        stroke-width: 5;
        fill-opacity: 0.5
    }
    path.smallarc {
        fill: blue;
    }
    path.largearc {
        fill: green;
    }
    circle {
        fill: steelblue;
    }
    svg {
        border: 1px;
        border-style: solid;
    }
</style>

<svg width="400" height="200">
    <circle cx="50" cy="50" r="5" />
    <circle cx="50" cy="150" r="5" />
    <circle cx="50" cy="100" r="5" />
    <path d="M 50 50 A 4 1 0 0 1 50 100" class="smallarc"/>
    <path d="M 50 50 A 4 1 0 0 1 50 150" class="largearc"/>
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="400" height="200" style="border: 1px; border-style: solid" >
	<circle cx="50" cy="50" r="5" style="fill: steelblue;"/>
	<circle cx="50" cy="150" r="5" style="fill: steelblue;"/>
	<circle cx="50" cy="100" r="5" style="fill: steelblue;"/>
	<path d="M 50 50 A 4 1 0 0 1 50 100" style="stroke: red; stroke-width: 5; fill-opacity: 0.5; fill: blue;"/>
	<path d="M 50 50 A 4 1 0 0 1 50 150" style="stroke: red; stroke-width: 5; fill-opacity: 0.5; fill: green;"/>
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg13.png[width=25%,pdfwidth=25%]
endif::[]

For an explanation of the `large-arc-flag` and `sweep-flag`, please see the tutorial mentioned above. The image below shows their effect on an arc:

image:svg_arc_flags.png[]

=== Groups and transformations
When putting elements (like circles) on the screen, we can define their positions in `cx` and `cy` using the coordinate system that they end up with. Say we want to have 5 circles in a `+` pattern, like so:

[source,html,linenums]
----
<style>
    circle {
        fill: steelblue;
    }
</style>

<svg width="200" height="200">
    <circle cx="100" cy="100" r="5" />
    <circle cx="50" cy="100" r="5" />
    <circle cx="100" cy="50" r="5" />
    <circle cx="150" cy="100" r="5" />
    <circle cx="100" cy="150" r="5" />
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <circle cx="100" cy="100" r="5" style="fill:steelblue;" />
    <circle cx="50"  cy="100" r="5" style="fill:steelblue;" />
    <circle cx="100" cy="50"  r="5" style="fill:steelblue;" />
    <circle cx="150" cy="100" r="5" style="fill:steelblue;" />
    <circle cx="100" cy="150" r="5" style="fill:steelblue;" />
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg14.png[width=25%,pdfwidth=25%]
endif::[]

Instead of setting the center of the `+` at `(100,100)` it might be easier to set it at `(0,0)` and define the other points relative to that position, and then shift (translate) everything to afterwards. We can do this using the *`transform`* directive. `transform` can be applied to almost any HTML element, but here we will first create a *group* around the circles so that we only need to transform the group instead of every circle separately.

[source,html,linenums]
----
<style>
    circle {
        fill: steelblue;
    }
</style>

<svg width="200" height="200">
    <g transform="translate(100,100)">
        <circle cx="0"   cy="0"   r="5" />
        <circle cx="-50" cy="0"   r="5" />
        <circle cx="0"   cy="50"  r="5" />
        <circle cx="50"  cy="0"   r="5" />
        <circle cx="0"   cy="-50" r="5" />
    </g>
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <g transform="translate(100,100)">
        <circle cx="0" cy="0" r="5" style="fill:steelblue;" />
        <circle cx="-50" cy="0" r="5" style="fill:steelblue;" />
        <circle cx="0" cy="50" r="5" style="fill:steelblue;" />
        <circle cx="50" cy="0" r="5" style="fill:steelblue;" />
        <circle cx="0" cy="-50" r="5" style="fill:steelblue;" />
    </g>
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg15.png[width=25%,pdfwidth=25%]
endif::[]

Apart from translation, `transform` let's us also scale, and rotate elements, as well as combine any of these. In the example below, the red square gets translated (green), rotated (blue), scaled (black) and scaled after rotation (orange). Note that in the last case, it _does_ matter in which order you perform the transformations: scaling after rotation is not the same as rotation after scaling.

[source,html,linenums]
----
<style>
    rect {
        fill-opacity: 0.5;
        stroke: none;
    }
    rect.base {
        fill: red;
    }
    rect.translated {
        fill: green;
    }
    rect.rotated {
        fill: blue;
    }
    rect.scaled {
        fill: black;
    }
    rect.combined {
        fill: orange;
    }
</style>
<svg width="200" height="200">
    <g transform="translate(50,50)">
        <rect x="0" y="0" width="50" height="50" class="base"/>
        <rect x="0" y="0" width="50" height="50" class="translated"
                    transform="translate(50,50)"/>
        <rect x="0" y="0" width="50" height="50" class="rotated"
                    transform="rotate(45)"/>
        <rect x="0" y="0" width="50" height="50" class="scaled"
                    transform="scale(0.5)"/>
        <rect x="0" y="0" width="50" height="50" class="combined"
                    transform="translate(100,0) scale(0.5)"/>
    </g>
</svg>
----

ifndef::backend-pdf[]
++++
<svg width="200" height="200">
    <g transform="translate(50,50)">
        <rect x="0" y="0" width="50" height="50" style="fill-opacity: 0.5; stroke: none; fill: red;"/>
        <rect x="0" y="0" width="50" height="50" style="fill-opacity: 0.5; stroke: none; fill: green;"
                    transform="translate(50,50)"/>
        <rect x="0" y="0" width="50" height="50" style="fill-opacity: 0.5; stroke: none; fill: blue;"
                    transform="rotate(45)"/>
        <rect x="0" y="0" width="50" height="50" style="fill-opacity: 0.5; stroke: none; fill: black;"
                    transform="scale(0.5)"/>
		<rect x="0" y="0" width="50" height="50" style="fill-opacity: 0.5; stroke: none; fill: orange;"
					transform="translate(100,0) scale(0.5)"/>
    </g>
</svg>
++++
endif::[]

ifdef::backend-pdf[]
image:svg16.png[width=25%,pdfwidth=25%]
endif::[]

=== Creating SVG using javascript
Up to now we have only shown a few datapoints, and hard-coded their positions. Obviously, this is not the approach to use when we work with more than these few datapoints. This is where javascript comes in again. Let's create a scatterplot with 10 random points. We create a `scatterplot.js` file with the following content:

[source,javascript,linenums]
----
var my_plot = document.getElementById('my_plot')
for ( var i = 0; i < 10; i++ ) {
    let newElement = document.createElementNS('http://www.w3.org/2000/svg','circle')
    newElement.setAttribute('cx', Math.floor(Math.random()*300));
    newElement.setAttribute('cy', Math.floor(Math.random()*300));
    newElement.setAttribute('r','20');
    my_plot.appendChild(newElement);
}
----

and the following HTML file:

[source,html,linenums]
----
<html>
<head>
    <style>
        svg {
            border: 1px;
            border-style: solid;
        }
        circle {
            fill: steelblue;
            opacity: 0.5;
        }
        circle:hover {
            fill: red;
        }
    </style>
</head>
<body>
    <svg id="my_plot" width="300px" height="300px">
        <!-- This is empty! -->
    </svg>
    <script src="scatterplot.js"></script>
</body>
</html>
----

What's happening?

In `scatterplot.js`:

* In line 1, we extract the element in the HTML file with id `my_plot`.

IMPORTANT: We need to put the reference to the external script at the _bottom_ of the HTML code, because the element with that ID needs to exist before you try to access it.

* We loop 10 times, where each time we
** create a new `circle` element
** give it a random `x` and `y` position
** give it a radius `r`
** append that new element to `my_plot`

The result should look like this (hover over the points for interactivity):

ifndef::backend-pdf[]
++++
<style>
    svg {
        border: 1px;
        border-style: solid;
    }
    circle.figthree {
        fill: steelblue;
        opacity: 0.5;
    }
    circle.figthree:hover {
        fill: red;
    }
</style>
<svg id="my_plot" width="300px" height="300px">
    <!-- This is empty! -->
</svg>
<script>
var my_plot = document.getElementById('my_plot')
for ( var i = 0; i < 10; i++ ) {
    let newElement = document.createElementNS('http://www.w3.org/2000/svg','circle')
    newElement.setAttribute('cx', Math.floor(Math.random()*300));
    newElement.setAttribute('cy', Math.floor(Math.random()*300));
    newElement.setAttribute('r','20');
    newElement.setAttribute("class","figthree")
    my_plot.appendChild(newElement);
}
</script>
++++
endif::[]

ifdef::backend-pdf[]
image:svg17.png[width=25%,pdfwidth=25%]
endif::[]

So even though the `index.html` file has an _empty_ `svg` element, we _do_ see the circles on the screen. Check the source code in the developer tools: you will see 10 `circle` elements nested within the `svg`.

image:svg_in_console.png[width=50%,pdfwidth=50%]

This looks complicated, but will become much simpler once we start using svelte.

=== Exercises
[#exercises_basicvis,sidebar,role=assignment]
--
Here are some exercises related to this chapter:

* SVG: https://svelte.dev/repl/bbe83abbe89f4e00a1a9d0b87f55b555?version=3.59.1
--