== Resources
- http://svelte.dev[Svelte homepage]
- http://kit.svelte.dev[SvelteKit homepage]
- http://svelte.dev/tutorial[Svelte tutorial]
- http://learn.svelte.dev[Svelte/Sveltekit tutorial]
- http://svelte.dev/docs[Svelte documentation]
- http://kit.svelte.dev/docs[SvelteKit documentation]

- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors[Overview of all available CSS selectors]
- https://developer.mozilla.org/en-US/docs/Web/SVG/Element[Overview of all available SVG elements with arguments]
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide[Javascript guide]
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference[Javascript reference]